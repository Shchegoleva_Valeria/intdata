package ru.svs.files;

import java.io.*;

/**
 * Данный класс выбирает из всех чисел только шестизначные.
 *
 * @author Щеголева В., группа 15/18
 */
public class IntDataSix {
    public static void main(String[] args) throws IOException {
        DataInputStream input = new DataInputStream(new FileInputStream(new File("//home/lera/IdeaProjects/IntData/src/ru/svs/files/", "intdata.dat")));
        DataOutputStream output = new DataOutputStream(new FileOutputStream(new File("//home/lera/IdeaProjects/IntData/src/ru/svs/files/", "int6data.dat")));

        while (input.available() >= 0) {
            int number = input.readInt();
            if (number > 99999 && number < 1000000) {
                output.writeInt(number);
            }
        }
        input.close();
        output.close();
    }
}
