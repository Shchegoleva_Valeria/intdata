package ru.svs.files;

import java.io.*;

/**
 * Данный класс создает файл целых чисел.
 *
 * @author Щеголева В., группа 15/18
 */
public class IntData {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File("//home/lera/IdeaProjects/IntData/src/ru/svs/files/", "intdata.txt")));
        DataOutputStream output = new DataOutputStream((new FileOutputStream(new File("//home/lera/IdeaProjects/IntData/src/ru/svs/files/", "intdata.dat"))));
        String number;

        while ((number = reader.readLine()) != null) {
            output.writeInt(Integer.valueOf(number));
        }

        reader.close();
        output.close();

    }
}
