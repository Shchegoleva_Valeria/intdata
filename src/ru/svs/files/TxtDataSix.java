package ru.svs.files;

import java.io.*;

/**
 * Данный класс выбирает счастливое число из всех шестизначных.
 *
 * @author Щеголева В.,т группа 15/18
 */
public class TxtDataSix {
    public static void main(String[] args) throws IOException {
        DataInputStream input = new DataInputStream(new FileInputStream(new File("//home/lera/IdeaProjects/IntData/src/ru/svs/files/", "int6data.dat")));
        BufferedWriter output = new BufferedWriter(new FileWriter(new File("//home/lera/IdeaProjects/IntData/src/ru/svs/files/", "txt6data.dat")));

        while (input.available() > 0) {

            int n = input.readInt();
            if (isLuckyNumber(n)) {
                output.write(String.valueOf(n) + "\n");
            }


        }
        input.close();
        output.close();
    }

    /**
     * Метод проверяет счастливое ли число путем сложения суммы первых трех цифр и последних трех цифр.
     *
     * @param n число
     * @return true если сумма первых трех цифр равно сумме последних трех, иначе - false
     */
    public static boolean isLuckyNumber(int n) {
        int n1 = n % 10;
        int n2 = n % 100;
        n2 = n2 / 10;
        int n3 = n % 1000;
        n3 = n3 / 100;
        int n4 = n % 10000;
        n4 = n4 / 1000;
        int n5 = n % 100000;
        n5 = n5 / 10000;
        int n6 = n % 1000000;
        n6 = n6 / 100000;
        return (n6 + n5 + n4 == n3 + n2 + n1);

    }
}
